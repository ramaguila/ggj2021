using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    public AudioClip backgroundMusic;

    public AudioClip[] m_sfx;

    [SerializeField] private AudioSource m_bgmAudioSource;
    [SerializeField] private AudioSource m_sfxAudioSource;
    [SerializeField] private AudioClip lastAudioClip;
    
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        Instance = this;
        m_bgmAudioSource.clip = backgroundMusic;
        m_bgmAudioSource.Play();
    }

    public void PlayBGM(AudioClip p_sfx)
    {
        m_bgmAudioSource.Stop();
        
        m_bgmAudioSource.clip = backgroundMusic;
            
        m_bgmAudioSource.Play();
        
    }

    public void PlaySFX(AudioClip p_sfx)
    {
        m_sfxAudioSource.PlayOneShot(p_sfx);
    }
}