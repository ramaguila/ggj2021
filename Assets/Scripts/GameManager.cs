using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public GameObject m_boyCharacter;
    public GameObject m_girlCharacter;
    public bool isBoyCharActive;

    private List<PlayerController> _playerControllers = new List<PlayerController>();
   [SerializeField] private KeyCode characterSwap = KeyCode.Tab;

   [SerializeField] private List<GameObject> m_girlsPlatformsLevel1;
   [SerializeField] private List<GameObject> m_girlsPlatformsLevel2;
   [SerializeField] private List<GameObject> m_girlsPlatformsLevel3;
   
   [SerializeField] private List<Platforms> m_platformsLevel1;
   [SerializeField] private List<Platforms> m_platformsLevel2;
   [SerializeField] private List<Platforms> m_platformsLevel3;

   [SerializeField] private PlayerController m_player;

   [SerializeField] private Transform m_respawnPointLocation;

   [SerializeField] private int m_currentLevel = 1;
   private int m_fragmentPoints = 0;

   [SerializeField] int maxFragmentPoints;

   [SerializeField] private TextMeshProUGUI m_fragmentsText;
    private void Awake()
    {
        Instance = this;
        // _playerControllers.Add(m_boyCharacter.GetComponent<PlayerController>());
        // _playerControllers.Add(m_girlCharacter.GetComponent<PlayerController>());
    }

    private void Start()
    {
        m_currentLevel = 1;
    }

    void Update()
    {
        DisplayFragment();
    }
    
    private void PlayerControllerSetActive(string p_charName)
    {
        if (p_charName.Equals("BOY"))
        {
            _playerControllers[0].SetCharacterActive(true);
            _playerControllers[1].SetCharacterActive(false);
        }
        else
        {
            _playerControllers[0].SetCharacterActive(false);
            _playerControllers[1].SetCharacterActive(true);
        }
    }
    public void SetActiveCharacter(GameObject p_character)
    {
        CameraController.Instance.SetActiveCharacter(p_character);
    }

    public void SetRespawnPoint(Transform p_respawnPointLocation)
    {
        m_respawnPointLocation = p_respawnPointLocation;
    }

    public Vector3 GetRespawnPoint()
    {
        return m_respawnPointLocation.position;
    }

    public void StopTime(bool p_timeStatus)
    {
        switch (m_currentLevel)
        {
            case 1:
                SetMovingPlatformStatus(m_platformsLevel1, p_timeStatus);
                break;
            case 2:
                SetMovingPlatformStatus(m_platformsLevel2, p_timeStatus);
                break;
            case 3:
                SetMovingPlatformStatus(m_platformsLevel3, p_timeStatus);
                break;
        }
    }

    public void CheckCurrentActivePlayer(bool p_isBoyActive)
    {
        if (p_isBoyActive)
        {
            switch (m_currentLevel)
            {
                case 1:
                    SetPlatformStatus(m_girlsPlatformsLevel1, false);
                    break;
                case 2:
                    SetPlatformStatus(m_girlsPlatformsLevel2, false);
                    break;
                case 3:
                    SetPlatformStatus(m_girlsPlatformsLevel3, false);
                    break;
            }
        }
        else
        {
            switch (m_currentLevel)
            {
                case 1:
                    SetPlatformStatus(m_girlsPlatformsLevel1, true);
                    break;
                case 2:
                    SetPlatformStatus(m_girlsPlatformsLevel2, true);
                    break;
                case 3:
                    SetPlatformStatus(m_girlsPlatformsLevel3, true);
                    break;
            }
        }
    }
    
    private void SetMovingPlatformStatus(List<Platforms> p_platformList, bool p_status)
    {
        foreach (Platforms platform in p_platformList)
        {
            platform.movingPlatform.ToggleMovement(p_status);
            platform.vanishingPlatform.ToggleVanish(p_status);
        }
    }

    private void SetPlatformStatus(List<GameObject> p_platformList, bool p_status)
    {
        foreach (GameObject platform in p_platformList)
        {
            platform.SetActive(p_status);
        }
    }
    
    

    public void AddLevel()
    {
        m_currentLevel++;
    }
    
    public void AddFragmentPoints()
    {
        m_fragmentPoints++;
        if (m_fragmentPoints == 7)
        {
            SceneManager.LoadScene("Scenes/Puzzle");
        }
    }

    void DisplayFragment()
    {
        m_fragmentsText.text = m_fragmentPoints + " / " + maxFragmentPoints;
    }




}

[Serializable]
public class Platforms
{
    public MovingPlatform movingPlatform;
    public VanishingPlatform vanishingPlatform;
}
