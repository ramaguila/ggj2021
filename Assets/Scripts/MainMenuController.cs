using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_titleText;
    [SerializeField] private Button m_playButton;
    [SerializeField] private Button m_exitButton;
    [SerializeField] private Button m_exitConfirmButton;
    [SerializeField] private Button m_exitDeclineButton;
    [SerializeField] private Button m_exitBoundsButton;
    [SerializeField] private GameObject m_exitPopup;

    private void OnEnable()
    {
        AddButtonListeners();
    }

    private void OnDisable()
    {
        RemoveButtonListeners();
    }

    private void AddButtonListeners()
    {
        m_playButton.onClick.AddListener(OnClickPlay);
        m_exitButton.onClick.AddListener(OnClickExit);
        m_exitBoundsButton.onClick.AddListener(OnClickExitDecline);
        m_exitDeclineButton.onClick.AddListener(OnClickExitDecline);
        m_exitConfirmButton.onClick.AddListener(OnClickExitConfirm);
    }

    private void RemoveButtonListeners()
    {
        m_playButton.onClick.RemoveAllListeners();
    }

    private void OnClickPlay()
    {
        SceneManager.LoadScene("Scenes/NewLevel1");
    }
    
    private void OnClickExit()
    {
       // m_exitPopup.SetActive(true);
       Application.Quit();
    }

    private void OnClickExitDecline()
    {
        m_exitPopup.SetActive(false);
    }

    private void OnClickExitConfirm()
    {
        Application.Quit();
    }
    
    
}
