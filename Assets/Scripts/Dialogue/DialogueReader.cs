using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueReader : MonoBehaviour
{
    public static DialogueReader Instance;
    [SerializeField] private TextMeshProUGUI m_text;
    [SerializeField] private string dialogue;

    private bool isTyping;
    private float speed;

    private void Awake()
    {
        Instance = this;
        speed = 0.025f;
    }

    public void PlayDialogue(string dialogue)
    {
        StartCoroutine(PlayText(dialogue));
    }

    IEnumerator PlayText(string p_textString)
    {
        dialogue = p_textString;
        m_text.text = string.Empty;
        isTyping = true;

        foreach (char c in dialogue) 
        {
            m_text.text += c;
            yield return new WaitForSeconds (speed);
        }
        isTyping = false;
    }

    public void StopSubtitle()
    {
        StopAllCoroutines();
        m_text.text = string.Empty;
    }


    public void OnClickContinue()
    {
        StopAllCoroutines();
        m_text.text = dialogue;
        isTyping = false;
    }

    public bool IsTyping()
    {
        return isTyping;
    }
    
}
