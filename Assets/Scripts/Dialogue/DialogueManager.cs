using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    public static DialogueManager Instance;
    public DialogueScriptable dialogueList;

    private int currentDialogueIndex = 0;

    private void Awake()
    {
        Instance = this;
    }

    public void PlayDialogue()
    {
        DialogueReader.Instance.PlayDialogue(dialogueList.dialogues[currentDialogueIndex]);
        currentDialogueIndex++;
    }
    
    
    
}
