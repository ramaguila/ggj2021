using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{

    [SerializeField] private Transform emptySpace;

    [SerializeField] private Camera camera;
    // Start is called before the first frame update
    
    [SerializeField] PuzzleTile[] tiles;

    [SerializeField] private GameObject PuzzleParent;

    [SerializeField] private SpriteRenderer spriteEnding;

    [SerializeField] private Sprite[] spriteList;

    private int emptySpaceIndex;

    private bool endingScenePlaying = false;

    private bool startGame = false;

    void Start()
    {
        startGame = false;
        emptySpaceIndex = 8;
        Shuffle();
    
      
    }
   
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
            if (hit)
            {
               // Debug.Log("hit");
                float a = Vector2.Distance(emptySpace.position, hit.transform.position);
                Debug.Log(a);
                if(Vector2.Distance(emptySpace.position, hit.transform.position) < 4)
                {
                   // Debug.Log("swap");
                    Vector2 lastEmptySpacePosition = emptySpace.position;
                    PuzzleTile tile = hit.transform.GetComponent<PuzzleTile>();
                    emptySpace.position = tile.targetPosition;
                    tile.targetPosition = lastEmptySpacePosition;
                    int tileIndex = findIndex(tile);
                    tiles[emptySpaceIndex] = tiles[tileIndex];
                    tiles[tileIndex] = null;
                    emptySpaceIndex = tileIndex;
                }
                
                
            }
        }

        if (CheckIfAllCorrect() && !endingScenePlaying && startGame)
        {
            StartCoroutine(Ending());
        }
    }

    IEnumerator DelayChecking()
    {
        Debug.Log("start game");
        yield return new WaitForSeconds(5);
        startGame = true;

    }

    void Shuffle()
    {
        for (int i = 0; i <= 7; i++)
        {
            var lastPos = tiles[i].targetPosition;
            int randomIndex = Random.Range(0, 7);
            tiles[i].targetPosition = tiles[randomIndex].targetPosition;
            tiles[randomIndex].targetPosition = lastPos;
            var tile = tiles[i];
            tiles[i] = tiles[randomIndex];
            tiles[randomIndex] = tile;
        }

        StartCoroutine(DelayChecking());
    }

    public int findIndex(PuzzleTile ts)
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            if (tiles[i] != null)
            {
                if (tiles[i] == ts)
                {
                    return i;
                }
            }
        }

        return -1;
    }

    bool CheckIfAllCorrect()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            if (tiles[i] != null)
            {
                if (!tiles[i].IsCorrectPos())
                {
                    return false;
                }
            }
        }

        return true;
    }

    IEnumerator Ending()
    {
        PuzzleParent.SetActive(false);
        endingScenePlaying = true;
        spriteEnding.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        spriteEnding.sprite = spriteList[1];
        yield return new WaitForSeconds(3);
        spriteEnding.sprite = spriteList[2];
        
    }
        
    
}
