using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTile : MonoBehaviour
{
    public Vector3 targetPosition;

    private Vector3 correctPosition;

    [SerializeField] private SpriteRenderer sprite;

    private bool isCorrectPos = true;

    public int number;
    // Start is called before the first frame update
    void Awake()
    {
        targetPosition = transform.position;
        correctPosition = transform.position;
        sprite.color = Color.white;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetPosition, 0.05f);
        if (targetPosition == correctPosition)
        {
            sprite.color = Color.white;
            isCorrectPos = true;
        }
        else
        {
            sprite.color = Color.gray;
            isCorrectPos = false;
        }
    }

    public bool IsCorrectPos()
    {
        return isCorrectPos;
    }
}
