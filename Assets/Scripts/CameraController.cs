using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;
    
    public Camera m_mainCamera;
    public bool isDelayed = false;
    
    [SerializeField] private GameObject m_activeCharacter;
    [SerializeField] private int offset = -10;
    [SerializeField] private int offsetY = 0;
    [SerializeField] private int offsetX = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void LateUpdate()
    {
        if (isDelayed)
        {
            FollowWithDelay();
        }
        else
        {
            Follow();
        }
    }

    private void FollowWithDelay()
    {
        float interpolation = 2 * Time.deltaTime;
        
        Vector3 position = this.transform.position;
        position.y = Mathf.Lerp(this.transform.position.y, m_activeCharacter.transform.position.y - offsetY, interpolation);
        position.x = Mathf.Lerp(this.transform.position.x, m_activeCharacter.transform.position.x - offsetX, interpolation);
        position.z = offset;
        
        this.transform.position = position;
    }

    private void Follow()
    {
        this.transform.position = new Vector3(m_activeCharacter.transform.position.x - offsetX,
            m_activeCharacter.transform.position.y - offsetY, offset);
    }

    public void SetActiveCharacter(GameObject p_character)
    {
        m_activeCharacter = p_character;
    }
    
}
