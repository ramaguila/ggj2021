using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    public string characterName;
    public float speedGirl = 10;
    public float speedBoy = 10;
    public bool isActive;

    [SerializeField] private int orbCount = 1;
    [SerializeField] private Rigidbody2D rb2d;
    
    [SerializeField] float jumpForceGirl;
    [SerializeField] float jumpForceBoy;
    [SerializeField] float jumpForceDirectionBoyValue;

    [SerializeField] private bool isSuperStrengthActive;
    [SerializeField] private bool isBoyActive;
    [SerializeField] private KeyCode characterSwap = KeyCode.Tab;


    private SpriteRenderer sr;

    private bool needToAddForce = false;

 
    private bool isJumping = false;


    private bool midAirSwitch = false;

    private float switchMoveHorizontal;

    private float speedBoyOriginal;

    private List<string> skillsObtain;

    [SerializeField] private SkeletonAnimation m_animation;

    private string currentAnimation = PlayerAnimations.Boy_Idle.ToString();

    private bool facingRight = true;
    private bool isPushing = false;
    
    private void Awake()
    {
        sr = this.GetComponent<SpriteRenderer>();
        Debug.Log(characterName);
        Instance = this;
        speedBoyOriginal = speedBoy;
         skillsObtain = new List<string>();
         facingRight = true;
    }

    private void FixedUpdate()
    {
        if (isActive)
        {
            Movement();
           
        }
    }
    void Update()
    {
        if (isActive)
        {
            Jump();
            CharacterSwitch();
        }
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            if(!isBoyActive) TimeStop(true);
        }
        
        if (Mathf.Abs(rb2d.velocity.y) < 0.001f)
        {
            isJumping = false;
            midAirSwitch = false;
            needToAddForce = false;
            if (isBoyActive)
            {
                // if (currentAnimation == PlayerAnimations.Boy_Jump.ToString())
                // {
                //    
                //     SetAnimation(PlayerAnimations.Boy_Run.ToString(), true);
                //     currentAnimation = PlayerAnimations.Boy_Run.ToString();
                // }
            }
            else
            {
                // if (currentAnimation == PlayerAnimations.Girl_Jump.ToString())
                // {
                //     isJumping = false;
                //     SetAnimation(PlayerAnimations.Girl_Run.ToString(), true);
                //     currentAnimation = PlayerAnimations.Girl_Run.ToString();
                // }
            }
        }

        
      
     
        
    }

    void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;

            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }

    private void Movement()
    {
        //Store the current horizontal input in the float moveHorizontal.
        float moveHorizontal = Input.GetAxis("Horizontal");

        //Store the current vertical input in the float moveVertical.
        float moveVertical = Input.GetAxis("Vertical");

        //Use the two store floats to create a new Vector2 variable movement.
        Vector2 movement = new Vector2(moveHorizontal, 0);

        //Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
        //rb2d.AddForce(movement * speed, ForceMode2D.Force);

    
        if (isBoyActive)
        {
            rb2d.velocity = new Vector2(speedBoy * moveHorizontal, rb2d.velocity.y);
            if (rb2d.velocity.x >= 0.001f || rb2d.velocity.x <= -0.001f)
            {
                if (speedBoy < speedBoyOriginal)
                {
                    if (currentAnimation != PlayerAnimations.Boy_Push.ToString())
                    {
                        SetAnimation(PlayerAnimations.Boy_Push.ToString(), true);
                        currentAnimation = PlayerAnimations.Boy_Push.ToString();
                    }
                }
                else
                {
                    if (currentAnimation != PlayerAnimations.Boy_Run.ToString() && Mathf.Abs(rb2d.velocity.y) < 0.1f)
                    {
                        SetAnimation(PlayerAnimations.Boy_Run.ToString(), true);
                        currentAnimation = PlayerAnimations.Boy_Run.ToString();
                    }
                }
             
            }
            else
            {
                
                
                if (currentAnimation != PlayerAnimations.Boy_Idle.ToString() && Mathf.Abs(rb2d.velocity.y) < 0.1f)
                {
                    SetAnimation(PlayerAnimations.Boy_Idle.ToString(), true);
                    currentAnimation = PlayerAnimations.Boy_Idle.ToString();
                }
            }
        }
       else
       {
           if (rb2d.velocity.x >= 0.001f || rb2d.velocity.x <= -0.001f)
           {
               if (currentAnimation != PlayerAnimations.Girl_Run.ToString() && Mathf.Abs(rb2d.velocity.y) < 0.1f )
               {
                   SetAnimation(PlayerAnimations.Girl_Run.ToString(), true);
                   currentAnimation = PlayerAnimations.Girl_Run.ToString();
               }
           }
           else
           {
               if (currentAnimation != PlayerAnimations.Girl_Idle.ToString()  && Mathf.Abs(rb2d.velocity.y) < 0.1f)
               {
                   SetAnimation(PlayerAnimations.Girl_Idle.ToString(), true);
                   currentAnimation = PlayerAnimations.Girl_Idle.ToString();
               }
           }
           rb2d.velocity = new Vector2(speedGirl * moveHorizontal, rb2d.velocity.y);
       }
        
        Flip(moveHorizontal);
        
       
    }
    
    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isJumping == false)
        {
            isJumping = true;
           
            if (isBoyActive)
            {
                currentAnimation = PlayerAnimations.Boy_Jump.ToString();
                SetAnimation(PlayerAnimations.Boy_Jump.ToString(), false);
              
                if (CheckObtainSkills(Skill.HighJump.ToString()))
                {
                    rb2d.AddForce(Vector2.up * (jumpForceBoy), ForceMode2D.Impulse);
                }
                else
                {
                    rb2d.AddForce(Vector2.up * jumpForceGirl, ForceMode2D.Impulse);
                }
            }
            else
            {
                currentAnimation = PlayerAnimations.Girl_Jump.ToString();
                SetAnimation(PlayerAnimations.Girl_Jump.ToString(), false);
                rb2d.AddForce(Vector2.up * jumpForceGirl, ForceMode2D.Impulse);
            }
        }
        
    }

    private void SuperStrength()
    {
        if (isSuperStrengthActive)
        {
            speedBoy = 1.0f;
        }
        else
        {
            speedBoy = speedBoyOriginal;
        }
    }

    private void CharacterSwitch()
    {
        if (Input.GetKeyDown(characterSwap))
        {
            isBoyActive = !isBoyActive;
            if (CheckObtainSkills(Skill.ThirdEye.ToString()))
            {
                GameManager.Instance.CheckCurrentActivePlayer(isBoyActive);
            }

            if (CheckObtainSkills(Skill.HighJump.ToString()))
            {
                if (isJumping && midAirSwitch == false)
                {
                    switchMoveHorizontal = Input.GetAxis("Horizontal");
                    midAirSwitch = true;
                    if (isBoyActive && Mathf.Abs(rb2d.velocity.y) > 0.001f &&
                        (switchMoveHorizontal > 0 || switchMoveHorizontal < 0))
                    {
                        needToAddForce = true;
                    }
                    else if (isBoyActive)
                    {
                      //  currentAnimation = PlayerAnimations.Boy_Jump.ToString();
                       // SetAnimation(PlayerAnimations.Boy_Jump.ToString(), false);
                       //if level 1
                      //  rb2d.AddForce(Vector2.up * (jumpForceBoy * 0.7f), ForceMode2D.Impulse);
                      //if level 2
                      rb2d.AddForce(Vector2.up * (jumpForceBoy * 20 * Time.fixedDeltaTime), ForceMode2D.Impulse);
                        
                        needToAddForce = false;
                    }
                }
            }

            if (isBoyActive)
            {
                currentAnimation = PlayerAnimations.Boy_Idle.ToString();
                SetAnimation(PlayerAnimations.Boy_Idle.ToString(), false);
                sr.color = Color.blue;
            }
            else
            {
                currentAnimation = PlayerAnimations.Girl_Idle.ToString();
                SetAnimation(PlayerAnimations.Girl_Idle.ToString(), false);
                sr.color = Color.red;
            }
        }
        if (needToAddForce && Mathf.Abs(rb2d.velocity.y) > 0.001f)
        {
            if (switchMoveHorizontal > 0)
            {
                rb2d.AddForce(Vector2.Lerp(Vector2.right, Vector2.right * jumpForceDirectionBoyValue, Time.fixedDeltaTime),
                    ForceMode2D.Force);
            }
            else
            {
                rb2d.AddForce(Vector2.Lerp(-Vector2.right, -Vector2.right * jumpForceDirectionBoyValue, Time.fixedDeltaTime),
                    ForceMode2D.Force);
            }
        }
    }

    public void SetSuperStrength(bool p_status)
    {
        isSuperStrengthActive = p_status;
        SuperStrength();
    }
    
    public void SetCharacterActive(bool activeStatus)
    {
        isActive = activeStatus;
    }

    public void ReturnToRespawnPoint()
    {
        this.transform.position = GameManager.Instance.GetRespawnPoint();
    }

    public bool IsBoyActive()
    {
        return isBoyActive;
    }
    
    private void TimeStop(bool p_status)
    {
        Debug.Log("Timestop " + p_status);
        GameManager.Instance.StopTime(p_status);
    }

    public void PickUpSkillOrb(Enum p_skill)
    {
        switch (p_skill)
        {
            case Skill.HighJump:
                skillsObtain.Add(p_skill.ToString());
                break;
            case Skill.ThirdEye:
                skillsObtain.Add(p_skill.ToString());
                break;
            case Skill.Strength:
                skillsObtain.Add(p_skill.ToString());
                break;
            case Skill.Grow:
                skillsObtain.Add(p_skill.ToString());
                break;
        }
    }

    public bool CheckObtainSkills(string p_value)
    {
        // return true;
        if (skillsObtain.Contains(p_value))
        {
            return true;
        }
        return false;
    }

    public void SetAnimation(string p_animation, bool p_loop)
    {
       // return;
        m_animation.skeleton.SetToSetupPose();
        m_animation.skeleton.SetBonesToSetupPose();
        m_animation.skeleton.SetSlotsToSetupPose();
        
        m_animation.state.SetAnimation(0, p_animation, p_loop);
    }

    // private void OnCollisionStay2D(Collision2D col)
    // {
    //     Debug.Log(PlayerController.Instance.IsBoyActive());
    //
    //     if (col.gameObject.tag == "HeavyObject" && isBoyActive)
    //     {
    //         if (currentAnimation != PlayerAnimations.Boy_Push.ToString() && !isPushing)
    //         {
    //             isPushing = true;
    //             currentAnimation = PlayerAnimations.Boy_Push.ToString();
    //             SetAnimation(PlayerAnimations.Boy_Push.ToString(), true);
    //         }
    //     }
    // }
    //
    // private void OnCollisionExit2D(Collision2D col)
    // {
    //     if (col.gameObject.tag == "HeavyObject" && isBoyActive)
    //     {
    //         isPushing = false;
    //     }
    // }

}

public enum Skill
{
    HighJump,
    ThirdEye,
    Strength,
    Grow,
    Time
}

public enum PlayerAnimations
{
    Boy_Idle,
    Boy_Jump,
    Boy_Push,
    Boy_Run,
   Girl_Idle,
    Girl_Jump,
    Girl_Push,
    Girl_Run,
  
}

