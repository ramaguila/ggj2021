using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddLevelPoint : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            GameManager.Instance.AddLevel();
            Destroy(gameObject);
        }
    }
}
