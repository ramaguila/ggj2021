using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyObject : MonoBehaviour
{
    private Rigidbody2D _rigidbody2D;
    private void Awake()
    {
        _rigidbody2D = this.GetComponent<Rigidbody2D>();
        _rigidbody2D.bodyType = RigidbodyType2D.Static;
    }

    // private void OnCollisionStay2D(Collision2D col)
    // {
    private void OnTriggerStay2D(Collider2D col)
    {
        Debug.Log(PlayerController.Instance.IsBoyActive());
        
       // if (col.gameObject.tag == "Player")
        if (col.tag.Equals("Player"))
        {
            if (PlayerController.Instance.IsBoyActive() && PlayerController.Instance.CheckObtainSkills(Skill.Strength.ToString()))
            {
                _rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
                PlayerController.Instance.SetSuperStrength(true);
            }
            else
            {
                _rigidbody2D.bodyType = RigidbodyType2D.Static;
            }
        }
        
    }
    private void OnTriggerExit2D(Collider2D col)
    {
    //private void OnCollisionExit2D(Collision2D col)
  //  {
       // if (col.gameObject.tag == "Player")
        if (col.tag.Equals("Player"))
        {
            if (PlayerController.Instance.IsBoyActive())
            {
                _rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            }

            PlayerController.Instance.SetSuperStrength(false);
        }
        
        _rigidbody2D.bodyType = RigidbodyType2D.Static;
    }
}
