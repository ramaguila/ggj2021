using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float time = 2f;
    private Transform m_transform;
    [SerializeField] private Vector3[] scales;


    private void Awake()
    {
        m_transform = this.GetComponent<Transform>();
        MyPingPong(true);
    }

    private void MyPingPong(bool isToBig)
    {
        if (isToBig)
        {
            m_transform.DOMove(scales[0], time).OnComplete(() => MyPingPong(false));
        }
        else
        {
            m_transform.DOMove(scales[1], time).OnComplete(() => MyPingPong(true));
        }
        
    }

    public void ToggleMovement(bool p_status)
    {
        if (p_status)
        {
            DOTween.PlayAll();
        }
        else
        {
            DOTween.PauseAll();
        }
    }
}
