using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillOrb : MonoBehaviour
{
    [SerializeField] private Skill skillOrb;
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            bool isBoyActive = PlayerController.Instance.IsBoyActive();
            switch (skillOrb)
            {
                case Skill.HighJump:
                    if(isBoyActive)
                    SkillObtain(true);
                    break;
                case Skill.ThirdEye:
                    if (!isBoyActive)
                    {
                        SkillObtain(true);
                        GameManager.Instance.CheckCurrentActivePlayer(isBoyActive);
                    }
                    break;
                case Skill.Strength:
                    if(isBoyActive)
                        SkillObtain(true);
                    break;
                case Skill.Grow:
                    if(!isBoyActive)
                        SkillObtain(true);
                    break;
            }
           
        }
    }

    void SkillObtain(bool p_value)
    {
        if (p_value)
        {
            PlayerController.Instance.PickUpSkillOrb(skillOrb);
            Destroy(gameObject);
        }
    }
}
