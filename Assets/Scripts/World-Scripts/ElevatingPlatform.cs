using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ElevatingPlatform : MonoBehaviour
{
    private Transform m_transform;
    public float time = 1f;
    [SerializeField] private float[] scales = {1, 3};

    public bool hatdog;

    private void Awake()
    {
        m_transform = this.GetComponent<Transform>();
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        
        if (col.tag.Equals("Player"))
        {
            if (!PlayerController.Instance.IsBoyActive() && PlayerController.Instance.CheckObtainSkills(Skill.Grow.ToString()))
            {
                ToggleElevate(scales[1]);
            }
            else
            {
                ToggleElevate(scales[0]);
            }
        }
    }
    
    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            ToggleElevate(scales[0]);
        }
    }

    private void Update()
    {
        // if (hatdog)
        // {
        //     ToggleElevate(scales[1]);
        // }
        // else
        // {
        //     ToggleElevate(scales[0]);
        // }
    }

    private void ToggleElevate(float scale)
    {
        m_transform.DOScaleY(scale, time);
    }
}
