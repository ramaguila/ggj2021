using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class RespawnPoint : MonoBehaviour
{
  
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag.Equals("Player"))
        {
            GameManager.Instance.SetRespawnPoint(this.transform);
        }
    }
}
